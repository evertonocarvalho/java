package test;

import java.util.ArrayList;
import java.util.List;

public class ListStreamTest {

	public static void main(String[] args) throws Exception {
		List<String> lista = new ArrayList<>();

		lista.add("um");
		lista.add(", dois");
		lista.add(", tr�s.");

		lista.stream().forEach(s -> {
			System.out.print(s.toString());
		});
	}

}
