package test;

/**
 * @author Everton Carvalho [evertonocarvalho@gmail.com]
 */
public class StringUniqueCharactersTest {

	public static void main(String[] args) {
		System.out.println(isUniqueChars("everton"));
	}

	/**
	 * Verifica se os caracteres da String s�o �nicos utilizando opera��es
	 * bitwise.<br>
	 * Opera��es bitwise n�o diferenciam caracteres mai�sculos e min�sculos.
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isUniqueChars(String string) {
		int checker = 0;

		for (int i = 0; i < string.length(); i++) {
			int val = string.charAt(i) - 'a';

			if ((checker & (1 << val)) > 0) {
				return false;
			}

			checker |= (1 << val);
		}

		return true;
	}

}
