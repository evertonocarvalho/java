package test;

public class DocesPorNotaTest {

	public static void main(String[] args) {
		System.out.println(new DocesPorNotaTest().candy(new int[] { 2, 1, 2, 3, 1, 3, 2, 1, 2, 3}));//20 candies
	}

	public int candy(int[] ratings) {
	    int tamanhoArrayNotas = ratings.length;
	    int[] arrayEsquerdaDireita = new int[tamanhoArrayNotas];
	    int[] arrayDireitaEsquerda = new int[tamanhoArrayNotas];

	    arrayEsquerdaDireita[0] = 1;

	    for (int i = 1; i < tamanhoArrayNotas; i++) {
	    	arrayEsquerdaDireita[i] = ratings[i] > ratings[i - 1] ? arrayEsquerdaDireita[i - 1] + 1 : 1;
	    }

	    arrayDireitaEsquerda[tamanhoArrayNotas - 1] = 1;

	    for (int i = tamanhoArrayNotas - 2; i >= 0; i--) {
	    	arrayDireitaEsquerda[i] = ratings[i] > ratings[i + 1] ? arrayDireitaEsquerda[i + 1] + 1 : 1;
	    }

	    int doces = 0;

	    for (int i = 0; i < tamanhoArrayNotas; i++) {
	    	doces += Math.max(arrayEsquerdaDireita[i], arrayDireitaEsquerda[i]);
	    }

	    return doces;
	}

}
