package br.edu.devmedia.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.edu.devmedia.dao.NotaDAO;
import br.edu.devmedia.dao.NotaHistoricoDAO;
import br.edu.devmedia.entidade.Nota;
import br.edu.devmedia.entidade.NotaHistorico;

/**
 * @author Everton [evertonocarvalho@gmail.com]
 */
@Path("/notas")
public class NotaService {

	private NotaDAO notaDAO;
	private NotaHistoricoDAO notaHistoricoDAO;

	@PostConstruct
	private void init() {
		notaDAO = new NotaDAO();
		notaHistoricoDAO = new NotaHistoricoDAO();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Nota> listarNotas() {
		List<Nota> lista = null;

		try {
			lista = notaDAO.listarNotas();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	@POST
	@Path("/inserir")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String inserirNota(Nota nota) {
		String msg = "";

		try {
			long idGerado = notaDAO.inserirNota(nota);
			msg = "Nota inserida com sucesso. O id da nota gerado � [" + idGerado + "]";
		}
		catch (Exception e) {
			msg = "Erro ao inserir a nota.";
			e.printStackTrace();
		}

		return msg;
	}

	@GET
	@Path("/get/{id}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Nota buscarPorId(@PathParam("id") long idNota) {
		Nota nota = null;

		try {
			nota = notaDAO.buscarNotaPorId(idNota);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return nota;
	}

	@PUT
	@Path("/atualizar/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String atualizarNota(Nota nota, @PathParam("id") long idNota) {
		String msg = "";

		try {
			nota.setId(idNota);
			notaDAO.atualizarNota(nota);
			msg = "Nota atualizada com sucesso.";
		}
		catch (Exception e) {
			msg = "Erro ao atualizar a nota.";
			e.printStackTrace();
		}

		return msg;
	}

	@DELETE
	@Path("/remover/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String removerNota(@PathParam("id") long idNota) {
		String msg = "";

		try {
			notaDAO.removerNota(idNota);
			msg = "Nota removida com sucesso.";
		}
		catch (Exception e) {
			msg = "Erro ao remover a nota.";
			e.printStackTrace();
		}

		return msg;
	}

	@GET
	@Path("/historico/{idNota}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NotaHistorico> listarHistoricoNota(@PathParam("idNota") long idNota) {
		List<NotaHistorico> lista = null;

		try {
			lista = notaHistoricoDAO.listarHistoricoNota(idNota);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

}
