package br.edu.devmedia.entidade;

import lombok.Data;

/**
 * @author Everton [evertonocarvalho@gmail.com]
 */
public @Data class Nota {

	private long id;
	private String titulo;
	private String descricao;

}
