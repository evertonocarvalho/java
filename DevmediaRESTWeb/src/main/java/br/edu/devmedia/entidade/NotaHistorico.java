package br.edu.devmedia.entidade;

import lombok.Data;

/**
 * @author Everton [evertonocarvalho@gmail.com]
 */
public @Data class NotaHistorico {

	private long id;
	private long idNota;
	private String dataOperacao;
	private String operacao;
	private String novoTitulo;
	private String antigoTitulo;
	private String novaDescricao;
	private String antigaDescricao;

}
