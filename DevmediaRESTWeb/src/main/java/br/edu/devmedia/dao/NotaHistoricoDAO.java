package br.edu.devmedia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.edu.devmedia.config.DBConfig;
import br.edu.devmedia.entidade.NotaHistorico;
import lombok.Cleanup;

/**
 * @author Everton [evertonocarvalho@gmail.com]
 */
public class NotaHistoricoDAO {

	/**
	 * @param idNota
	 * @return
	 * @throws Exception
	 */
	public List<NotaHistorico> listarHistoricoNota(long idNota) throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_nota_historico WHERE id_nota = ? ORDER BY dt_operacao DESC";

		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setLong(1, idNota);

		ResultSet rs = statement.executeQuery();

		List<NotaHistorico> lista = new ArrayList<>();

		while (rs.next()) {
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setAntigaDescricao(rs.getString("antiga_descricao"));
			notaHistorico.setAntigoTitulo(rs.getString("antigo_titulo"));
			notaHistorico.setDataOperacao(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(rs.getTimestamp("dt_operacao")));
			notaHistorico.setId(rs.getLong("id"));
			notaHistorico.setIdNota(rs.getLong("id_nota"));
			notaHistorico.setNovaDescricao(rs.getString("nova_descricao"));
			notaHistorico.setNovoTitulo(rs.getString("novo_titulo"));
			notaHistorico.setOperacao(rs.getString("operacao"));
			lista.add(notaHistorico);
		}

		return lista;
	}

}
