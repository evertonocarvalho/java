package br.edu.devmedia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.edu.devmedia.config.DBConfig;
import br.edu.devmedia.entidade.Nota;
import lombok.Cleanup;

/**
 * @author Everton [evertonocarvalho@gmail.com]
 */
public class NotaDAO {

	public List<Nota> listarNotas() throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_nota";

		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet rs = statement.executeQuery();

		List<Nota> lista = new ArrayList<>();

		while (rs.next()) {
			Nota nota = new Nota();
			nota.setId(rs.getLong("id_note"));
			nota.setTitulo(rs.getString("titulo"));
			nota.setDescricao(rs.getString("descricao"));
			lista.add(nota);
		}

		return lista;
	}

	public Nota buscarNotaPorId(long idNota) throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "SELECT * FROM tb_nota WHERE id_note = ?";

		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setLong(1, idNota);

		ResultSet rs = statement.executeQuery();

		Nota nota = null;

		if (rs.next()) {
			nota = new Nota();
			nota.setId(rs.getLong("id_note"));
			nota.setTitulo(rs.getString("titulo"));
			nota.setDescricao(rs.getString("descricao"));
		}

		return nota;
	}

	/**
	 * @param nota Nota que ser� persistida.
	 * @return ID gerado da Nota.
	 * @throws Exception
	 */
	public long inserirNota(Nota nota) throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "INSERT INTO tb_nota(titulo, descricao) VALUES (?, ?)";

		PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, nota.getTitulo());
		statement.setString(2, nota.getDescricao());

		statement.execute();

		ResultSet rs = statement.getGeneratedKeys();
		long idGerado = 0;

		if (rs.next()) {
			idGerado = rs.getLong(1);
		}

		return idGerado;
	}

	public boolean atualizarNota(Nota nota) throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "UPDATE tb_nota SET titulo = ?, descricao = ? WHERE id_note = ?";

		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, nota.getTitulo());
		statement.setString(2, nota.getDescricao());
		statement.setLong(3, nota.getId());

		return statement.execute();
	}

	public boolean removerNota(long idNota) throws Exception {
		@Cleanup Connection connection = DBConfig.getConnection();

		String sql = "DELETE FROM tb_nota WHERE id_note = ?";

		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setLong(1, idNota);

		return statement.execute();
	}

}
