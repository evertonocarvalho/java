package br.edu.devmedia.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * Filtro para for�ar o charset UTF-8 em requisi��es e respostas.
 * 
 * @author Everton [evertonocarvalho@gmail.com]
 */
@Provider
public class UTF8RequestAndResponseFilter implements ContainerRequestFilter, ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		responseContext.getHeaders().putSingle("Content-Type", forceUTF8ContentType(responseContext.getMediaType()));
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		requestContext.getHeaders().putSingle("Content-Type", forceUTF8ContentType(requestContext.getMediaType()));
	}

	/**
	 * @param mediaType
	 * @return
	 */
	private String forceUTF8ContentType(MediaType mediaType) {
		if (mediaType != null) {
			String contentType = mediaType.toString();

			if (!contentType.contains("charset")) {
				return contentType = contentType + ";charset=utf-8";
			}

			return contentType;
		}

		return "";
	}

}
