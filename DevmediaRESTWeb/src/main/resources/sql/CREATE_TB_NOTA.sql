CREATE TABLE `tb_nota` (
  `id_note` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(10) DEFAULT NULL,
  `descricao` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_note`)
)