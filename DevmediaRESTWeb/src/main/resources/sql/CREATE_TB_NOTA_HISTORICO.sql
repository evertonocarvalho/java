CREATE TABLE `tb_nota_historico` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_nota` bigint(20) NOT NULL,
  `dt_operacao` datetime NOT NULL,
  `operacao` varchar(10) NOT NULL,
  `novo_titulo` varchar(10) DEFAULT NULL,
  `antigo_titulo` varchar(10) DEFAULT NULL,
  `nova_descricao` varchar(30) DEFAULT NULL,
  `antiga_descricao` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
)