USE `notes_db`;

DELIMITER $$

DROP TRIGGER IF EXISTS notes_db.tg_after_insert_tb_nota$$
CREATE
	TRIGGER `notes_db`.`tg_after_insert_tb_nota`
    AFTER INSERT ON `tb_nota`
    FOR EACH ROW BEGIN
	INSERT INTO tb_nota_historico(id_nota, dt_operacao, operacao, novo_titulo, antigo_titulo, nova_descricao, antiga_descricao)
	VALUES (NEW.id_note, NOW(), 'INSERT', NEW.titulo, NULL, NEW.descricao, NULL);
END$$

DROP TRIGGER IF EXISTS notes_db.tg_after_update_tb_nota$$
CREATE
	TRIGGER `notes_db`.`tg_after_update_tb_nota`
    AFTER UPDATE ON `tb_nota`
    FOR EACH ROW BEGIN
	INSERT INTO tb_nota_historico(id_nota, dt_operacao, operacao, novo_titulo, antigo_titulo, nova_descricao, antiga_descricao)
	VALUES (NEW.id_note, NOW(), 'UPDATE', NEW.titulo, OLD.titulo, NEW.descricao, OLD.descricao);
END$$

DROP TRIGGER IF EXISTS notes_db.tg_after_delete_tb_nota$$
CREATE
	TRIGGER `notes_db`.`tg_after_delete_tb_nota`
    AFTER DELETE ON `tb_nota`
    FOR EACH ROW BEGIN
	INSERT INTO tb_nota_historico(id_nota, dt_operacao, operacao, novo_titulo, antigo_titulo, nova_descricao, antiga_descricao)
	VALUES (OLD.id_note, NOW(), 'DELETE', NULL, OLD.titulo, NULL, OLD.descricao);
END$$
DELIMITER ;